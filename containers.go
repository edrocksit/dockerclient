package dockerclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type containerPort struct {
	PrivatePort int
	PublicPort  int
	Type        string
	IP          string
}

type Container struct {
	Id         string
	Image      string
	Command    string
	Status     string
	Ports      []containerPort
	SizeRw     int
	SizeRootFs int
}

type ContainerConfig struct {
	Hostname     string
	User         string
	Memory       int
	MemorySwap   int
	AttachStdin  bool
	AttachStdout bool
	AttachStderr bool
	//PortSpecs containerPort
	Tty          bool
	OpenStdin    bool
	StdinOnce    bool
	Env          []string
	Cmd          []string
	Dns          []string
	Image        string
	Volumes      map[string]interface{}
	VolumesFrom  string
	WorkingDir   string
	ExposedPorts []containerPort
}

type PortBinding map[string][]map[string]string

type HostConfig struct {
	Binds           []string
	LxcConf         map[string]string
	PortBindings    map[string]PortBinding
	PublishAllPorts bool
	Privileged      bool
}

type ContainerInspection struct {
	//info about a container
}

//Returns a pointer to a new container
func NewContainer() *Container {
	return &Container{}
}

//Creates the container on the remote docker server
func (c *Container) Create(name string, config ContainerConfig, client *Client) error {
	b, err := json.Marshal(config)
	if err != nil {
		return err
	}
	body := bytes.NewReader(b)

	apiUrl, err := client.url.Parse("/containers/create")
	if err != nil {
		return err
	}

	if len(name) > 0 {
		q := url.Values{}
		q.Set("name", name)
		apiUrl.RawQuery = q.Encode()
	}

	resp, err := http.Post(apiUrl.String(), "application/json", body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(string(r))
	v := make(map[string]interface{})
	if err := json.Unmarshal(r, &v); err != nil {
		return err
	}
	c.Id = v["Id"].(string)
	return nil
}

//Starts the container if it is not already running
func (c *Container) Start(hc *HostConfig, client *Client) error {
	var body *bytes.Reader
	if hc != nil {
		b, err := json.Marshal(hc)
		if err != nil {
			return err
		}
		body = bytes.NewReader(b)
	}

	apiUrl, err := client.url.Parse("/containers/" + c.Id + "/start")
	if err != nil {
		return err
	}
	resp, err := http.Post(apiUrl.String(), "application/json", body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return statusCodeToError(resp.StatusCode)
}

//Stops the container if it is running
func (c *Container) Stop(t time.Duration, client *Client) error {
	q := url.Values{}
	q.Set("t", strconv.Itoa(int(t.Seconds())))

	apiUrl, err := client.url.Parse("/containers/" + c.Id + "/stop")
	if err != nil {
		return err
	}
	apiUrl.RawQuery = q.Encode()

	resp, err := http.Post(apiUrl.String(), "", nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return statusCodeToError(resp.StatusCode)
}

//Restarts the container
//If it is not running it will start the container
func (c *Container) Restart(t time.Duration, client *Client) error {
	q := url.Values{}
	q.Set("t", strconv.Itoa(int(t.Seconds())))

	apiUrl, err := client.url.Parse("/containers/" + c.Id + "/restart")
	if err != nil {
		return err
	}
	apiUrl.RawQuery = q.Encode()

	resp, err := http.Post(apiUrl.String(), "", nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return statusCodeToError(resp.StatusCode)
}

//Kills the container if it is running
func (c *Container) Kill(client *Client) error {
	apiUrl, err := client.url.Parse("/containers/" + c.Id + "/kill")
	if err != nil {
		return err
	}

	resp, err := http.Post(apiUrl.String(), "", nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return statusCodeToError(resp.StatusCode)
}

//Removes the container if it exists
func (c *Container) Remove(removeAssociatedVolumes, forceRemoval bool, client *Client) error {
	q := url.Values{}
	q.Set("v", strconv.FormatBool(removeAssociatedVolumes))
	q.Set("force", strconv.FormatBool(forceRemoval))

	apiUrl, err := client.url.Parse("/containers/" + c.Id)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("DELETE", apiUrl.String(), nil)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return statusCodeToError(resp.StatusCode)
}

//Inspects the container
func (c *Container) Inspect() (ContainerInspection, error) {
	return ContainerInspection{}, nil
}

func statusCodeToError(code int) error {
	switch code {
	case http.StatusNoContent:
		return nil
	case http.StatusBadRequest:
		return errors.New("Bad parameters")
	case http.StatusNotFound:
		return errors.New("No such container")
	case http.StatusInternalServerError:
		return errors.New("Server error")
	default:
		return errors.New("StatusCode not handled error:" + strconv.Itoa(code))
	}
}

//TODO: attach, wait, copy files/folders, resize pseudo-tty, export, inspect changes
//list processes running in container, create a new image from container changes
