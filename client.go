package dockerclient

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

type Client struct {
	url        *url.URL
	Containers []*Container
	Images     []*Image
}

func NewClient(endpoint string) (*Client, error) {
	url, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}
	return &Client{url: url}, nil
}

//Updates the current list of containers
func (c *Client) UpdateContainerList(showAll, showSize bool, since, before string, limit int) error {
	q := url.Values{}
	q.Set("all", strconv.FormatBool(showAll))
	q.Set("size", strconv.FormatBool(showSize))

	if len(since) > 0 {
		q.Set("since", since)
	}
	if len(before) > 0 {
		q.Set("before", before)
	}
	if limit > 0 {
		q.Set("limit", strconv.Itoa(limit))
	}

	err := c.ApiQuery("/containers/json", q, &c.Containers)
	if err != nil {
		return err
	}

	return nil
}

//Updates the current list of images
func (c *Client) UpdateImageList(showAll bool) error {
	q := url.Values{}
	q.Set("all", strconv.FormatBool(showAll))

	err := c.ApiQuery("/images/json", q, &c.Images)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) ApiQuery(path string, query url.Values, unmarshalInto interface{}) error {
	apiUrl, err := c.url.Parse("/containers/json")
	if err != nil {
		return err
	}
	apiUrl.RawQuery = query.Encode()

	resp, err := http.Get(apiUrl.String())
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	//TODO: handle status codes
	//possibly by returning an error about certain status codes
	//I think they are the same meaning for each code between the different calls
	//besides for the difference between images/containers
	//so you may have to say something like "no such image/container"
	//or maybe depending on the query i could make my own error struct
	//then set bool if it is an image or container
	//and the error method of the struct could fill it in boom done its golden idea
	// switch resp.StatusCode {
	// case 200:
	// 	continue
	// case 400, 500:
	// 	return
	// }

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(b, unmarshalInto)
	if err != nil {
		return err
	}
	return nil
}
