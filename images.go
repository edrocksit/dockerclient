package dockerclient

import (
	"os"
)

type Image struct {
	Id          string
	RepoTags    []string
	Created     int
	Size        int
	VirtualSize int
	ParentId    string
	Repository  string
	Tag         string
}

type ImageInspection struct {
	//info about an image
}

//Returns a pointer to a new image
func NewImage() *Image {
	return &Image{}
}

//Creates the image on the remote docker server
func (i *Image) Create() error {
	return nil
}

//Inspects the image
func (i *Image) Inspect() (ImageInspection, error) {
	return ImageInspection{}, nil
}

//Inserts a file into the image
func (i *Image) InsertFile(file *os.File) error {
	return nil
}

//Remove this image from the client
func (i *Image) Remove() error {
	return nil
}

//TODO: get history, push to registry, tag into registry, search images
